#ifndef _IMAGE_H_
#define _IMAGE_H_

#include <stdbool.h>
#include <stdint.h>

#define PIXEL_SIZE sizeof(struct pixel)


struct pixel { uint8_t b, g, r; };


struct image {
  uint64_t width, height;
  struct pixel* data;
};

bool create_image(struct image *addr, int width, int height);
void delete_image(struct image *img);

bool create_rotated_image(struct image src, struct image *dst);

#endif
