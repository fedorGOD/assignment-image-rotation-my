#include "formats.h"

#define BMP_BIT_COUNT_REQUIRED 24
#define BMP_SIGNATURE 19778
#define BMP_COMPRESSION_RGB 0
#define BMP_VERSION 40

static uint32_t bmp_get_row_padding(uint32_t width){
    uint32_t width_bytes = width * PIXEL_SIZE;
    if(width_bytes % 4 == 0) return 0;
    return 4 - (width_bytes % 4);
}

static enum read_status check_header(struct bmp_header header){
    if(header.bfType != BMP_SIGNATURE)
        return READ_INVALID_SIGNATURE;
    if(header.biBitCount != BMP_BIT_COUNT_REQUIRED)
        return READ_WRONG_BIT_COUNT;
    return READ_OK;
}

enum read_status read_bmp(FILE *f, struct image *addr){

    struct bmp_header header;

    if( fread(&header, sizeof(struct bmp_header), 1, f) != 1){
        return READ_CANNOT_READ;
    }

    enum read_status header_status = check_header(header);

    if(header_status != READ_OK)
        return header_status;
    
    uint32_t width, height;
    width = header.biWidth;
    height = header.biHeight;

    if(!create_image(addr, width, height)){
        return READ_CANNOT_ALLOC;
    }

    uint32_t padding = bmp_get_row_padding(width);

    for(uint32_t i=0; i<height; i++){
        struct pixel *dst = addr->data + (i * width);
        if(fread(dst, sizeof(struct pixel), width, f) != width){
            return READ_CANNOT_READ;
        }
        
        if(fseek(f, padding, SEEK_CUR)){
            return READ_CANNOT_READ;
        }
    }

    return READ_OK;
}

static struct bmp_header bmp_create_header(uint32_t width, uint32_t height){
    
    const size_t data_size = (PIXEL_SIZE * width + bmp_get_row_padding(width))  * height;
    
    struct bmp_header header;

    header.bfType = BMP_SIGNATURE;
    header.bfileSize = sizeof(struct bmp_header) + data_size;
    header.bfReserved = 0;
    header.bOffBits = sizeof(struct bmp_header);
    header.biSize = BMP_VERSION;
    header.biWidth = width;
    header.biHeight = height;
    header.biPlanes = 1;
    header.biBitCount = PIXEL_SIZE * 8;
    header.biCompression = BMP_COMPRESSION_RGB;
    header.biSizeImage = data_size;
    header.biXPelsPerMeter = 0;
    header.biYPelsPerMeter = 0;
    header.biClrUsed = 0;
    header.biClrImportant = 0;

    return header;
}


bool write_bmp(FILE *f, struct image img){
    struct bmp_header header = bmp_create_header(img.width, img.height);

    if(fwrite(&header, sizeof(struct bmp_header), 1, f) != 1)
        return false;

    const size_t padding = bmp_get_row_padding(img.width);
    const uint32_t padding_src = 0;

    for(int i=0; i<img.height; i++){
        const struct pixel *src = img.data + (img.width * i);

        if(fwrite(src, PIXEL_SIZE, img.width, f) != img.width)
            return false;
        if(fwrite(&padding_src, padding, 1, f) != 1)
            return false;
    }

    return true;
}
